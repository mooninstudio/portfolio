﻿using UnityEngine;
using UniRx;

/// <summary>
/// Obiekt głowy avatara VR
/// </summary>
public class VRAvatarHead : MonoBehaviour
{
	/// <summary>
	/// Kamera z perspektywy oczu
	/// </summary>
	[SerializeField] private Camera hmd;

	public Vector3 Position => hmd.transform.position;
	public Quaternion Rotation => hmd.transform.rotation;
	/// <summary>
	/// Urządzenie VR nakładane na głowę odpowiadające tej głowie
	/// </summary>
	private IVRInputDevice headset;

	public void Initialize(IVRInputDevice headset)
	{
		this.headset = headset;
	}
	/// <summary>
	/// Ustawia przesunięcie względem pozycji zwracanej przez urządzenie VR
	/// </summary>
	/// <param name="offset">Przesunięcie</param>
	public void SetPositionOffset(Vector3 offset)
	{
		transform.position = offset;
	}
	/// <summary>
	/// Ustawia, czy kamera "z oczu" ma być aktywna
	/// </summary>
	/// <param name="isActive">Czy kamera ma być aktywna?</param>
	public void SetCameraActive(bool isActive)
	{
		hmd.enabled = isActive;
	}
}
