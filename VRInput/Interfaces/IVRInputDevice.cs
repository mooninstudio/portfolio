﻿using UnityEngine;
using System;
using UniRx;

public interface IVRInputDevice
{
	IObservable<Unit> ConnectedStream { get; }
	IObservable<Unit> DisconnectedStream { get; }
	IObservable<Unit> TrackingAcquiredStream { get; }
	IObservable<Unit> TrackingStream { get; }
	IObservable<Unit> TrackingLostStream { get; }
	Vector3 Position { get; }
	Quaternion Rotation { get; }
	bool IsValid { get; }
}
