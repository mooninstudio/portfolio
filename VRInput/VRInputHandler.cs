﻿using UnityEngine.XR;
using System.Collections.Generic;
using System.Linq;
using System;
using UniRx;

/// <summary>
/// Odpowiada za przekazywanie informacji o urządzeniach VR
/// </summary>
public class VRInputHandler : IVRInputProvider
{
	/// <summary>
	/// Stream informujący o wciśnięciu dowolnego przycisku Trigger
	/// </summary>
	public IObservable<Unit> AnyTriggerDownStream => Observable.Merge(LeftHandNode.TriggerDownStream, RightHandNode.TriggerDownStream);
	/// <summary>
	/// Stream informujący o zwolnieniu dowolnego przycisku Trigger
	/// </summary>
	public IObservable<Unit> AnyTriggerUpStream => Observable.Merge(LeftHandNode.TriggerUpStream, RightHandNode.TriggerUpStream);
	/// <summary>
	/// Stream informujący o trzymaniu dowolnego przycisku Trigger
	/// </summary>
	public IObservable<Unit> AnyTriggerHeldStream => Observable.Merge(LeftHandNode.TriggerHeldStream, RightHandNode.TriggerHeldStream);

	/// <summary>
	/// Stream informujący o wciśnięciu dowolnego przycisku Grip
	/// </summary>
	public IObservable<Unit> AnyGripDownStream => Observable.Merge(LeftHandNode.GripDownStream, RightHandNode.GripDownStream);
	/// <summary>
	/// Stream informujący o zwolnieniu dowolnego przycisku Grip
	/// </summary>
	public IObservable<Unit> AnyGripUpStream => Observable.Merge(LeftHandNode.GripUpStream, RightHandNode.GripUpStream);
	/// <summary>
	/// Stream informujący o trzymaniu dowolnego przycisku Grip
	/// </summary>
	public IObservable<Unit> AnyGripHeldStream => Observable.Merge(LeftHandNode.GripHeldStream, RightHandNode.GripHeldStream);

	/// <summary>
	/// Stream informujący o wciśnięciu dowolnego przycisku Primary
	/// </summary>
	public IObservable<Unit> AnyPrimaryButtonDownStream => Observable.Merge(LeftHandNode.PrimaryButtonDownStream, RightHandNode.PrimaryButtonDownStream);
	/// <summary>
	/// Stream informujący o trzymaniu dowolnego przycisku Primary
	/// </summary>
	public IObservable<Unit> AnyPrimaryButtonHeldStream => Observable.Merge(LeftHandNode.PrimaryButtonHeldStream, RightHandNode.PrimaryButtonHeldStream);
	/// <summary>
	/// Stream informujący o zwolnieniu dowolnego przycisku Primary
	/// </summary>
	public IObservable<Unit> AnyPrimaryButtonUpStream => Observable.Merge(LeftHandNode.PrimaryButtonUpStream, RightHandNode.PrimaryButtonUpStream);

	/// <summary>
	/// Stream informujący o wciśnięciu dowolnego przycisku Secondary
	/// </summary>
	public IObservable<Unit> AnySecondaryButtonDownStream => Observable.Merge(LeftHandNode.SecondaryButtonDownStream, RightHandNode.SecondaryButtonDownStream);
	/// <summary>
	/// Stream informujący o trzymaniu dowolnego przycisku Secondary
	/// </summary>
	public IObservable<Unit> AnySecondaryButtonHeldStream => Observable.Merge(LeftHandNode.SecondaryButtonHeldStream, RightHandNode.SecondaryButtonHeldStream);
	/// <summary>
	/// Stream informujący o zwolnieniu dowolnego przycisku Secondary
	/// </summary>
	public IObservable<Unit> AnySecondaryButtonUpStream => Observable.Merge(LeftHandNode.SecondaryButtonUpStream, RightHandNode.SecondaryButtonUpStream);

	/// <summary>
	/// Urządzenie VR nakładane na głowę
	/// </summary>
	public Headset HeadsetNode { get; private set; }
	/// <summary>
	/// Urządzenie VR trzymane w lewej dłoni
	/// </summary>
	public Handheld LeftHandNode { get; private set; }
	/// <summary>
	/// Urządzenie VR trzymane w prawej dłoni
	/// </summary>
	public Handheld RightHandNode { get; private set; }

	IVRInputDevice IVRInputProvider.HeadsetNode => HeadsetNode;
	IHandheld IVRInputProvider.LeftHandNode => LeftHandNode;
	IHandheld IVRInputProvider.RightHandNode => RightHandNode;

	/// <summary>
	/// Aktualna lista wykrywanych urządzeń
	/// </summary>
	private List<InputDevice> devices = new List<InputDevice>();
	/// <summary>
	/// Lista wykrywanych urządzeń z poprzedniej klatki
	/// </summary>
	private List<InputDevice> _devices = new List<InputDevice>();

	public VRInputHandler()
	{
		HeadsetNode = new Headset();
		LeftHandNode = new Handheld();
		RightHandNode = new Handheld();

		UpdateDeviceList();
	}
	public void Update()
	{
		UpdateDeviceList();
	}

	/// <summary>
	/// Aktualizuje listę wykrywanych urządzeń
	/// </summary>
	private void UpdateDeviceList()
	{
		_devices.Clear();
		_devices.AddRange(devices);
		InputDevices.GetDevices(devices);

		devices.Except(_devices).ToList().ForEach(HandleNewDevice);
		devices.Union(_devices).ToList().ForEach(HandleDeviceUpdate);
		_devices.Except(devices).ToList().ForEach(HandleDeviceRemoved);
	}

	/// <summary>
	/// Obsługuje wykrycie nowego urządzenia
	/// </summary>
	private void HandleNewDevice(InputDevice device)
	{
		switch (device.role)
		{
			case InputDeviceRole.Generic: HeadsetNode.OnConnect(device); break;
			case InputDeviceRole.LeftHanded: LeftHandNode.OnConnect(device); break;
			case InputDeviceRole.RightHanded: RightHandNode.OnConnect(device); break;
			default: break;
		}
	}
	/// <summary>
	/// Obsługuje aktualizację urządzenia
	/// </summary>
	private void HandleDeviceUpdate(InputDevice device)
	{
		switch (device.role)
		{
			case InputDeviceRole.Generic: HeadsetNode.UpdateConnected(); break;
			case InputDeviceRole.LeftHanded: LeftHandNode.UpdateConnected(); break;
			case InputDeviceRole.RightHanded: RightHandNode.UpdateConnected(); break;
			default: break;
		}
	}
	/// <summary>
	/// Obsługuje rozłączenie urządzenia
	/// </summary>
	private void HandleDeviceRemoved(InputDevice device)
	{
		switch (device.role)
		{
			case InputDeviceRole.Generic: HeadsetNode.OnDisconnect(); break;
			case InputDeviceRole.LeftHanded: LeftHandNode.OnDisconnect(); break;
			case InputDeviceRole.RightHanded: RightHandNode.OnDisconnect(); break;
			default: break;
		}
	}
}
